/*

Activity Instructions:
1. Create an activity.js file on where to write and save the solution for the activity.

2. Find users with letter s in their first name or d in their last name.
- Use the $or operator.
- Show only the firstName and lastName fields and hide the _id field.

3. Find users who are from the HR department and their age is greater than or equal to 70.
- Use the $and operator
4. Find users with the letter e in their first name and has an age of less than or equal to 30.
- Use the $and, $regex and $lte operators

5. Create a git repository named S29.

6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.

7. Add the link in Boodle.

*/


// Find users with letter s in their first name or d in their last name
db.users.find({ $or: [{ firstName: { $regex: 's', $options: '$i'}}, { lastName: { $regex: 'd', $options: '$i'}}] });

// Find users who are from the HR department and their age is greater than or equal to 70
db.users.find({ $and: [{ department: 'HR' }, { age: { $gte: 70 } }] });

// Find users with the letter e in their first name and has an age of less than or equal to 30
db.users.find({ $and: [{ firstName: { $regex: 'e', $options: '$i' } }, { age: { $lte: 30 } }] });






